package com.hcl.variable;

public class Typecasting {

	public static void main(String[] args) {
		byte value = 5;
		int num = value;
		System.out.println("implicit" + num);
		
		int meters = 25;
		double amount = 65.23;
		
	meters = (int) amount;
		System.out.println("explicit" + meters);
		
		long input = 150;
		float input1 = input;
		System.out.println("special" +input1);
		
	}

}
