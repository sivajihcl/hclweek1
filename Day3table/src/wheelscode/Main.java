package wheelscode;

class Vehicle{
	public void noOfWheels() {
		System.out.println("No of wheels undefined");
	}
}
class scooter extends Vehicle{
	public void noOfWheels() {
		System.out.println("No of wheels 2");
	}
}
class car extends Vehicle{
	public void noOfWheels() {
		System.out.println("No of Wheels 4");
	}
}

public class Main {

	public static void main(String[] args) {
		Vehicle vehicle = new Vehicle();  // Create a Vehicle object
		
		Vehicle Scooter = new scooter();  // Create a Scooter object
		
		Vehicle Car = new car();  // Create a Car object
		
					vehicle.noOfWheels();     //call noOfWheels of vehicle class
					
					Scooter.noOfWheels();   //call noOfWheels of Scooter class
					
					Car.noOfWheels();         // call noOfWheels of Car class
	}

}
